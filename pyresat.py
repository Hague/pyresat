
import re
import sys

def tocap(lit : int) -> str:
    if lit < 0:
        return f"\\{-2 * lit - 1}"
    elif lit > 0:
        return f"\\{2 * lit}"
    else:
        raise ValueError("No 0 lits!")

assign_re = ""
test_re = ""

for line in sys.stdin:
    if line.startswith("c"):
        # c comment
        pass
    elif line.startswith("p"):
        # p cnf nvars nclauses
        nvars = int(line.split()[2])
        assign_re = "(?:()|())" * nvars
    elif line.startswith("%"):
        # % who knows why
        break
    else:
        # clause!
        lits = [ int(l) for l in line.split() ]
        clause = "|".join(tocap(l) for l in lits[:-1])
        test_re += f"(?:{clause})"

satre = assign_re + test_re

print(re.match(satre, "") is not None)
