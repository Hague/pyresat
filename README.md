
# Python RegEx SAT Solver

Pass input in [DIMACS format][dimacs] via stdin.

Constructs a regular expression that matches `""` iff the instance is satisfiable. Prints answer.

    $ cat test-sat.cnf | python pyresat.py
    True
    $ cat test-unsat.cnf | python pyresat.py
    False

Is not competitive.

[dimacs]: https://jix.github.io/varisat/manual/0.2.0/formats/dimacs.html
